from sopel import module

@module.event("JOIN")
@module.rule(".*")
def warn(bot, trigger):
    if trigger.sender in ("#ardour-osx", "#ardour-windows"):
        saynick = "" if trigger.nick == bot.nick else " " + trigger.nick
        bot.say("Hello{}. This channel is now deprecated; please "
                "join #ardour instead, and people will (may) help you there.".format(saynick))

