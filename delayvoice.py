from sopel import module
from sopel.config.types import StaticSection, ListAttribute, ValidatedAttribute
from sopel.tools import Identifier
import time, threading, queue, collections, re, itertools, os.path, json, random

DEFAULT_DELAY = 10
DEFAULT_RANGE = 10

class DelayVoiceSection(StaticSection):
    channels = ListAttribute("channels")
    message = ValidatedAttribute("message")
    use_pm = ValidatedAttribute("use_pm", bool, default=True)
    delay = ValidatedAttribute("delay", int)
    delayrange = ValidatedAttribute("delayrange", int)
    warndelay = ValidatedAttribute("warndelay", int)

def configure(config):
    config.define_section('delayvoice', DelayVoiceSection)
    config.delayvoice.configure_setting('channels',
                        "List of channels where delayvoice will act")
    config.delayvoice.configure_setting('message',
                        "Message to say when unvoiced people try to speak")
    config.delayvoice.configure_setting('delay',
                        "Time in seconds before someone who JOINed is voiced")
    config.delayvoice.configure_setting('warndelay',
                        "Time in seconds before someone who talks without "
                        "being voiced is warned")

class INFO(dict):
    def __init__(self):
        self._lock = threading.Lock()
        self._filename = None
    @property
    def filename(self):
        return self._filename
    @filename.setter
    def filename(self, value):
        with self._lock:
            if self._filename is None:
                try:
                    saved = json.load(open(value))
                except:
                    pass
                else:
                    for k, v in saved.items():
                        super().__setitem__(k, self.get(k, 0) + v)
            self._filename = value
            json.dump(self, open(self._filename, "w"))
    def incr(self, key, value=1):
        def later():
            with self._lock:
                super(self.__class__, self).__setitem__(
                        key, self.get(key, 0) + value)
                if self._filename is not None:
                    json.dump(self, open(self._filename, "w"))
        threading.Thread(target=later).start()
    def _bad(self, *a, **kw):
        raise TypeError("Only use the incr() method to mutate")
    __setitem__ = setdefault = update = _bad
    __delitem__ = pop = popitem = clear = _bad
    del _bad
INFO = INFO()

def setup(bot):
    bot.config.define_section('delayvoice', DelayVoiceSection)
    INFO.filename = os.path.join(bot.config.homedir, 'delayvoice.stats')

class async_voice:
    def __init__(self):
        self.pending = queue.Queue()
        self.lock = threading.Lock()
    def __call__(self, bot, channel, nicklist, value=True):
        for chunk in itertools.zip_longest(*([iter(nicklist)]*4)):
            self.pending.put( (channel, chunk, value) )
        def later():
            if not self.lock.acquire(timeout=1):
                return # another thread is still working for us
            while True:
                try:
                    channel, nicks, value = self.pending.get_nowait()
                except queue.Empty:
                    self.lock.release()
                    return
                try:
                    privs = bot.privileges[channel]
                except KeyError:
                    continue
                # filter out users that left or are are have the mode
                nicks = [ n for n in nicks
                            if n and n in privs
                               and bool(privs[n] & module.VOICE) != value ]
                if not nicks: continue
                time.sleep(1)
                pm = "+" if value else "-"
                bot.write(["MODE", channel, pm + "v"*len(nicks)] + nicks)
                if value: INFO.incr("voiced", len(nicks))
        threading.Thread(target=later).start()
async_voice = async_voice()

@module.thread(False)
@module.event("MODE")
@module.rule("$nickname")
def voiceall(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    try:
        modes, *targets = trigger.args[1:]
    except ValueError:
        return
    if not re.match("\+o+", modes): return
    if bot.nick not in map(Identifier, targets): return
    do_voiceall(bot, trigger.sender)

def do_voiceall(bot, channel):
    async_voice(
            bot, channel,
            [ nick for nick, priv in bot.privileges.get(channel, {}).items()
                   if not priv & module.VOICE ]
        )

voice_delays = collections.defaultdict(dict)

@module.thread(False)
@module.event("JOIN")
@module.rule(".*")
def voice(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    nick = trigger.nick
    if nick == bot.nick: return
    delay = bot.config.delayvoice.delay or DEFAULT_DELAY
    maxdelay = delay + (bot.config.delayvoice.delayrange or DEFAULT_RANGE)
    curdelay = random.randint (delay, maxdelay)
    voice_delays[trigger.sender][nick] = curdelay
    def later():
        time.sleep(curdelay)
        if nick not in bot.privileges.get(trigger.sender, ()): return # kicked
        bot.write(["MODE", trigger.sender, "+v", nick])
        INFO.incr("voiced")
    threading.Thread(target=later).start()

revoice_tries = collections.defaultdict(dict)

@module.thread(False)
@module.rule(".*")
def warn(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    nick = trigger.nick
    if nick == bot.nick: return
    if bot.privileges.get(trigger.sender, {}).get(nick, 0) & module.VOICE:
        # already voiced, no warning
        tries = revoice_tries[trigger.sender].get(nick, 0)
        if tries >= 1: # forget a revoice try
            revoice_tries[trigger.sender][nick] = tries - 1
        else:
            revoice_tries[trigger.sender].pop(nick, None)
        return
    if re.match(" *https?://[^ ]* *$", trigger.args[-1]):
        INFO.incr("maybe_spam")
        return # do not warn if this is only a link
    curdelay = voice_delays[trigger.sender].pop(nick, None)
    if curdelay is None: return
    def later():
        # wait to see if user is banned
        time.sleep(bot.config.delayvoice.warndelay or 0)
        privlist = bot.privileges.get(trigger.sender, {})
        if nick not in privlist:
            INFO.incr("spam_prevented")
            return # has been banned
        message = bot.config.delayvoice.message
        if message:
            message = message.format(delay=curdelay,
                                     nick=nick,
                                     channel=trigger.sender)
            if bot.config.delayvoice.use_pm:
                bot.say(message, nick)
            else:
                bot.reply(message) # reply in channel
        INFO.incr("unvoiced_speech")
    threading.Thread(target=later).start()

# Forget users when they are kicked or voiced to save memory

@module.thread(False)
@module.event("MODE")
@module.rule(".*")
def handle_voice(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    try:
        modes, *targets = trigger.args[1:]
    except ValueError:
        return
    if not re.match("\+v+$", modes): return
    for nick in map(Identifier, targets):
        voice_delays[trigger.sender].pop(nick, None)

@module.thread(False)
@module.event("KICK")
@module.rule(".*")
def handle_kick(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    voice_delays[trigger.sender].pop(Identifier(trigger.args[1]), None)
    INFO.incr("kicked")

# Try to revoice people that lost the voice flag, but do not insist too much

MAX_TRIES = 4

@module.thread(False)
@module.event("MODE")
@module.rule(".*")
def try_revoice(bot, trigger):
    if trigger.sender not in (bot.config.delayvoice.channels or []): return
    try:
        modes, *targets = trigger.args[1:]
    except ValueError:
        return
    if not re.match("\-v+$", modes): return
    INFO.incr("lost_voice", len(targets))
    to_voice = []
    for nick in map(Identifier, targets):
        tries = revoice_tries[trigger.sender].get(nick, 0)
        if tries < MAX_TRIES:
            revoice_tries[trigger.sender][nick] = tries + 1
            to_voice.append(nick)
    async_voice(bot, trigger.sender, to_voice)

# Actions

import pprint

@module.require_privmsg()
@module.require_admin()
@module.commands("delayvoice")
def handle_command(bot, trigger):
    command, *tail = (trigger.group(2) or "").strip().split(" ", 1)
    line = tail[0] if tail else ""
    if command == "stats":
        command = "dump"
        line = "INFO.{}".format(line) if line else "INFO"
    if command in ("dump", "debug"):
        if not line:
            obj = { k: globals()[k]
                    for k in ("lastsaid", "revoice_tries", "INFO") }
        else:
            try:
                obj = globals()[line]
            except KeyError:
                bot.say("Unknown variable {!r}".format(line))
                return
        for m in pprint.pformat(obj, depth=4).split("\n"):
            if m: bot.say(m)
    elif command == "voiceall":
        if not line:
            bot.say("I need you to tell me a channel")
            return
        do_voiceall(bot, Identifier(line))
    elif command == "unvoiceall":
        if not line:
            bot.say("I need you to tell me a channel")
            return
        channel = Identifier(line)
        async_voice( bot, line,
                     [ nick for nick, priv
                            in bot.privileges.get(channel, {}).items()
                            if priv & module.VOICE ],
                     value=False)
    else:
        bot.say("Unknown command")
