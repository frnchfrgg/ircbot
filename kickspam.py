from sopel import module
from sopel.config.types import StaticSection, ListAttribute, ValidatedAttribute
import re

class KickSpamSection(StaticSection):
    badness = ListAttribute("badness")
    message = ValidatedAttribute("message")

def configure(config):
    config.define_section('kickspam', KickSpamSection)
    config.kickspam.configure_setting('badness',
                                   "List of bad terms that trigger a kick")
    config.kickspam.configure_setting('message',
                                   "Message to say when kicking a spammer")

def setup(bot):
    bot.config.define_section('kickspam', KickSpamSection)

def info_badness(bot, badness):
    bot.reply("New value is {!r}. To commit to disk, "
              "use .save from the admin module.".format(badness))

@module.require_privmsg
@module.require_admin
@module.commands('badness_add')
@module.priority('low')
def addbadness(bot, trigger):
    badness = bot.config.kickspam.badness or []
    badness.append(trigger.match.group(2).strip())
    bot.config.kickspam.badness = badness
    info_badness(bot, badness)

@module.require_privmsg
@module.require_admin
@module.commands('badness_del')
@module.priority('low')
def delbadness(bot, trigger):
    try:
        n = int(trigger.match.group(2))
    except:
        return
    badness = bot.config.kickspam.badness or []
    del badness[n]
    bot.config.kickspam.badness = badness
    info_badness(bot, badness)

@module.rule(".*")
def maybekick(bot, trigger):
    badness = bot.config.kickspam.badness or []
    said = (trigger.args[-1] if trigger.args else '').lower()
    if any((term.lower() in said) for term in badness if term):
        bot.write(['KICK', trigger.sender, trigger.nick],
                   bot.config.kickspam.message or "No spam here.")
