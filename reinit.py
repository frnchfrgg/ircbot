from sopel import module, config
from sopel.modules.reload import f_reload
import signal

# The goal of these classes is to make f_reload do what we want
class Trigger(object):
    admin = True
    def group(self, x):
        return ""

class WrapBot(object):
    def __init__(self, bot):
        self._bot = bot

    def reply(self, *args):
        pass

    def __getattribute__(self, attr):
        if attr == "reply":
            return object.__getattribute__(self, attr)
        else:
            bot = object.__getattribute__(self, "_bot")
            return getattr(bot, attr)

def reinit(bot, do_reply=True):
    newconfig = config.Config(bot.config.filename)
    newconfig._is_daemonized = bot.config._is_daemonized # is it needed ?
    bot.config = newconfig
    if not do_reply: bot = WrapBot(bot)
    f_reload(bot, Trigger())

@module.commands("reinit")
@module.require_privmsg()
@module.require_admin()
@module.thread(False)
def cmd_reinit(bot, _):
    reinit(bot)

class CfgSection(config.types.StaticSection):
    signal = config.types.ValidatedAttribute("signal")

the_bot = None

def signaled_reinit(*args):
    print("Received reinit signal")
    if the_bot is not None:
        reinit(the_bot, do_reply=False)

def setup(bot):
    global the_bot
    the_bot = bot
    bot.config.define_section('reinit', CfgSection)
    sig = getattr(signal, bot.config.reinit.signal or "SIGUSR2", None)
    if sig is not None:
        signal.signal(sig, signaled_reinit)
