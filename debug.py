from sopel import module

LOG="/path/to/some/file/sopel/can/write/into"
LOG="/home/frnchfrgg/t.log"


class allmessages():
    f = open(LOG, "w")
    def __call__(self, bot, trigger):
        self.f.write(trigger.raw + "\n")
        self.f.write(repr([trigger.sender, trigger.nick, trigger.args]) + "\n")
        self.f.flush()
    def __setattr__(self, attr, value):
        if attr != "event":
            object.__setattr__(self, attr, value)
    class event():
        def __contains__(self, val):
            return True
        def __iter__(self):
            return iter(())
    event = event()
    rule = [".*"]

allmessages = allmessages()
