from sopel import module
from sopel.tools import Identifier
import re, collections, pprint, sys, os
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from confusables import Confusables

UNVOICED_FACTOR = 2     # when there is no explicit unvoiced cost
DEMERIT_UNVOICED = True # should be True when delayvoice is on
MAX_UNVOICED = 2        # when more messages said without voice, kick
KICK_SCORE = 10         # the score that triggers a kick
REHAB_SCORE = -2        # what voiced user get when no rule is triggered

letters = re.compile("(?!\d)\w")
nonletters = re.compile("([^\w\s]|\d)")
def too_many_nonletters(line):
    return len(line) > 5 and \
        len(letters.findall(line)) < len(nonletters.findall(line)) * 1.5

badness = ["https://williampitcock.com/",
           "https://bryanostergaard.com/",
           "https://encyclopediadramatica.rs/Freenodegate",
           "https://mattstrout.com/",
           "https://evestigatorsucks.com/",
           "ATTN: This channel has moved",
           "After the acquisition by Private Internet Access", 
           "Christel just posted",
           "kaniini has invited you",
           "rampjoulaboyrampjoulaboy",
           "DfZdPTy.jpg",
           "http://magaimg.net/img",
           "el0p0os7u7fz.jpg",
           "r2n8a788qs211.jpg",
           (re.compile("https?://(www\.)?youtube\.com", re.I), 2),
           (re.compile("we are not spamming.*"
                      "https?://(www\.)?youtube\.com", re.I), 6),
           (too_many_nonletters, KICK_SCORE/3, KICK_SCORE),
]

filters = []
def fudge():
    f = os.path.join(os.path.dirname(__file__), "confusables.txt")
    conf = Confusables(f)
    d = {":": "፡﹕", "/": "／", "n": "ᥒ", ".": "．", "e": "ᥱ"}
    for c, l in d.items():
        conf.confusables_dict.setdefault(c, []).extend(l)
    return conf.confusables_regex
fudge = fudge()
for entry in badness:
    try:
        # maybe the entry is (filter, voiced score, unvoiced score)
        filtr, voiced, unvoiced = entry
        voiced = int(voiced)
        unvoiced = int(unvoiced)
    except:
        try:
            # maybe the entry is (filter, score)
            filtr, voiced = entry
            voiced = int(voiced)
        except:
            # assume the entry is just a single filter
            filtr = entry
            voiced = KICK_SCORE
        unvoiced = voiced * UNVOICED_FACTOR
    if callable(filtr):
        pass
    elif callable(getattr(filtr, "search", None)):
        filtr = filtr.search
    else:
        try:
            filtr = re.compile(fudge(filtr), re.IGNORECASE).search
        except:
            continue
    filters.append((filtr, voiced, unvoiced))

scores = {}

def do_kick(bot, nick):
    nick = Identifier(nick)
    scores.pop(nick, None) # cleanup
    for channel, nicks in bot.privileges.items():
        if nick not in nicks: continue
        bot.write(['KICK', channel, nick], "Kindergarten is elsewhere!")

@module.thread(False)
@module.event("JOIN")
@module.rule(".*")
def voice(bot, trigger):
    # ensure that no leftover score from before is laying around
    scores.pop(trigger.nick, None)

@module.thread(False)
@module.rule(".*")
def kickban(bot, trigger):
    if not trigger.args: return
    if not trigger.nick in bot.privileges.get(trigger.sender, {}): return
    line = trigger.args[-1]
    if DEMERIT_UNVOICED and not (
            bot.privileges[trigger.sender][trigger.nick] & module.VOICE):
        matches = [incr for filtr, _, incr in filters if filtr(line)]
        change = sum(matches) if matches else 0
        if (MAX_UNVOICED or -1) >= 0:
            change += KICK_SCORE / (MAX_UNVOICED+1)
    else:
        matches = [incr for filtr, incr, _ in filters if filtr(line)]
        change = sum(matches) if matches else REHAB_SCORE
    newscore = scores.get(trigger.nick, 0) + change
    if newscore <= 0:
        scores.pop(trigger.nick, None) # cleanup
    elif newscore >= KICK_SCORE:
        do_kick(bot, trigger.nick)
    else:
        scores[trigger.nick] = newscore

@module.require_privmsg()
@module.require_admin()
@module.commands("spamscores")
def spamscores(bot, trigger):
    for m in pprint.pformat(scores, depth=4).split("\n"):
        if m: bot.say(m)

@module.thread(False)
@module.require_privmsg()
@module.require_admin()
@module.commands("spamkick")
def spamkick(bot, trigger):
    nick = Identifier((trigger.group(2) or "").strip())
    do_kick(bot, nick)
