# ircbot

This project contains several ad-hoc modules for the
[sopel IRC bot](https://sopel.chat/). They have been written with #ardour on
Freenode in mind, but can be used elsewhere.

These modules should go into the `.sopel/modules/` folder.

# Example configuration

The contents of `.sopel/default.cfg` could be

```
[core]
nick = thebotnick
host = irc.freenode.net
use_ssl = true
port = 6697
owner = myownnick
channels = #mychannel, #another, #athird
enable = reinit, delayvoice, kickspam

[kickspam]
badness = https://williampitcock.com/, https://bryanostergaard.com/, https://encyclopediadramatica.rs/Freenodegate, https://mattstrout.com/, https://evestigatorsucks.com/, ATTN: This channel has moved
message = Kindergarten is elsewhere! Check that it works

[delayvoice]
channels = #mychannel, #athird
message = Due to ongoing spam attacks, you'll need to wait {delay} seconds before anything you write here will be visible by others. Sorry for the inconvenience.  Welcome to #ardour!
#delay = 10
```
